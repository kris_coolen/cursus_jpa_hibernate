open module be.kriscoolen.CursusJPaHibernate {
    requires java.persistence;
    requires org.hibernate.orm.core;
    requires java.sql;
    requires net.bytebuddy;
    requires java.xml.bind;
    requires java.desktop;
}