package be.kriscoolen.hoofdstuk6.opdracht5;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class SaveCompany {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            Company company = new Company("COOLTECH");
            String[] staffNames={"Kris Coolen","Vera Paumen","Jan Paumen", "Thieu Coolen", "Hannelore Coolen"};
            int i=0;
            for(StaffRole role: StaffRole.values()){
                StaffMember staffMember = new StaffMember(role,staffNames[i]);
                company.addStaffMember(staffMember);
                i++;
            }
            tx.begin();
            em.persist(company);
            tx.commit();

        } finally {
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
