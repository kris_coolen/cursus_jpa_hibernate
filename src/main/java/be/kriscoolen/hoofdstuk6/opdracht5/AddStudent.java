package be.kriscoolen.hoofdstuk6.opdracht5;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.swing.*;

public class AddStudent {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            School school = em.find(School.class,1L);
            tx.commit();
            if(school!=null){
                String studentName = JOptionPane.showInputDialog("Enter student name to add to school " + school.getName());
                tx.begin();
                Student newStudent = new Student();
                newStudent.setName(studentName);
                school.addStudent(newStudent);
                tx.commit();
                tx.begin();
                school = em.find(School.class,1L);
                tx.commit();
                System.out.println("Students of school '"+school.getName()+"' (alphabetically sorted):");
                for(Student s: school.getStudents() ){
                    System.out.println(s.getName());
                }
            }else{
                System.out.println("school with id 1 not found in db");
            }

        } finally {
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
