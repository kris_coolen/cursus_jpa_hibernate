package be.kriscoolen.hoofdstuk6.opdracht5;

import be.kriscoolen.hoofdstuk6.opdracht4.Patient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GetSchool {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            School school = em.find(School.class,1L);
            tx.commit();
            System.out.println("Studenten van school " + school.getName() + ":");
            for(Student s: school.getStudents()){
                System.out.println(s.getName());
            }

        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
