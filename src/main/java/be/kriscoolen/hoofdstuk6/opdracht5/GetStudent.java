package be.kriscoolen.hoofdstuk6.opdracht5;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GetStudent {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Student student = em.find(Student.class,1L);
            tx.commit();
            System.out.println("Kenmerken van student met id " + student.getId() +":");
            System.out.println("naam student: " + student.getName());
            System.out.println("naam school: " +student.getSchool().getName());

        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
