package be.kriscoolen.hoofdstuk6.opdracht5;

import be.kriscoolen.hoofdstuk6.opdracht4.MedicalFile;
import be.kriscoolen.hoofdstuk6.opdracht4.Patient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class SaveSchool {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            School school = new School();
            school.setName("MOSART");
            Student student1 = new Student();
            student1.setName("Kris Coolen");
            Student student2 = new Student();
            student2.setName("Omar Schultink");
            school.addStudent(student2);
            school.addStudent(student1);
            tx.begin();
            em.persist(school);
            tx.commit();

        } finally {
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
