package be.kriscoolen.hoofdstuk6.opdracht8.beers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class RemoveBrewer {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;
        EntityTransaction entityTransaction = null;
        try{
            entityManagerFactory = Persistence.createEntityManagerFactory("course");
            entityManager=entityManagerFactory.createEntityManager();

            entityTransaction = entityManager.getTransaction();
            entityTransaction.begin();
            //search brewer 105
            Brewer brewer = entityManager.find(Brewer.class,105);
            entityTransaction.commit();
            if(brewer!=null){
                entityTransaction.begin();
                entityManager.remove(brewer);
                entityTransaction.commit();
            }
            else{
                System.out.println("brewer with id 105 already removed from db!");
            }

        }catch(Exception e){
            e.getStackTrace();
            entityTransaction.rollback();
        }
        finally{
            if(entityManager!=null) entityManager.close();
            if(entityManagerFactory!=null) entityManagerFactory.close();
        }
    }


}
