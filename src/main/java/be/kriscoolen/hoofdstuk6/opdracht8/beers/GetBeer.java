package be.kriscoolen.hoofdstuk6.opdracht8.beers;

import be.kriscoolen.hoofdstuk6.opdracht5.Student;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GetBeer {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Beer beer = em.find(Beer.class,4);
            tx.commit();
            System.out.println(beer);

        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
