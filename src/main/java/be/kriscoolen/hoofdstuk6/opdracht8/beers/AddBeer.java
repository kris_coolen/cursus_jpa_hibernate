package be.kriscoolen.hoofdstuk6.opdracht8.beers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class AddBeer {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Brewer brewer = em.find(Brewer.class,48);
            Category category = em.find(Category.class,49);
            Beer newBeer = new Beer();
            newBeer.setName("Lekker Bierke");
            newBeer.setPrice(1.60f);
            newBeer.setStock(1000);
            newBeer.setBrewer(brewer);
            newBeer.setCategory(category);
            newBeer.setAlcohol(6.6f);
            em.persist(newBeer);
            tx.commit();

        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
