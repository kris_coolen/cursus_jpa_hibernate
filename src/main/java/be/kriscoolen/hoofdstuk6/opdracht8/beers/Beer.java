package be.kriscoolen.hoofdstuk6.opdracht8.beers;

import javax.persistence.*;
import java.util.Arrays;

//@NamedQuery(name="findAllBeers",query = "select b from Beer as b")
@Entity
@Table(name="Beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name="Name")
    private String name;

    @Column(name="Price")
    private float price;

    @Column(name="Stock")
    private int stock;

    @Column(name="Alcohol")
    private float alcohol;

    @ManyToOne
    @JoinColumn(name="BrewerId")
    private Brewer brewer;

    @ManyToOne
    @JoinColumn(name="CategoryId")
    private Category category;

    @Column(name="Version")
    private int version;

    @Column(name="Image")
    @Basic(fetch = FetchType.LAZY)
    @Lob
    private byte[] image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(float alcohol) {
        this.alcohol = alcohol;
    }

    public Brewer getBrewer() {
        return brewer;
    }

    public void setBrewer(Brewer brewer) {
        this.brewer = brewer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", alcohol=" + alcohol +
                ", brewer=" + brewer +
                ", category=" + category +
                ", version=" + version +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}
