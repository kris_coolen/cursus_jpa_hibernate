package be.kriscoolen.hoofdstuk6.opdracht8.beers;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="Categories")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private int id;

    @Column(name="Category")
    private String name;

    @OneToMany(mappedBy = "category")
    private Set<Beer> beers = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Beer> getBeers() {
        return beers;
    }

    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }

    public void addBeer(Beer beer){
        beers.add(beer);

    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                //", beers=" + beers +
                '}';
    }
}
