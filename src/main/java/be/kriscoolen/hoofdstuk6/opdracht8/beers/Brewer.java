package be.kriscoolen.hoofdstuk6.opdracht8.beers;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="Brewers")
public class Brewer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private int id;

    @Column(name="Name")
    private String name;

    @Column(name="Address")
    private String address;

    @Column(name="ZipCode")
    private String zopCode;

    @Column(name="City")
    private String city;

    @Column(name="Turnover")
    private int turnover;

    @OneToMany(mappedBy = "brewer",orphanRemoval = true)
    private Set<Beer> beers = new HashSet<>();

    @Override
    public String toString() {
        return "Brewer{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode='" + zopCode + '\'' +
                ", city='" + city + '\'' +
                ", turnover=" + turnover +
               // ", beers=" + beers +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZopCode() {
        return zopCode;
    }

    public void setZopCode(String zopCode) {
        this.zopCode = zopCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getTurnover() {
        return turnover;
    }

    public void setTurnover(int turnover) {
        this.turnover = turnover;
    }

    public Set<Beer> getBeers() {
        return beers;
    }

    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }
}
