package be.kriscoolen.hoofdstuk6.opdracht8.beers;

import be.kriscoolen.hoofdstuk6.opdracht5.School;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GetBrewer {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Brewer brewer = em.find(Brewer.class,25);
            tx.commit();
            System.out.println(brewer);
            System.out.println("Beers of brewer "+brewer.getName()+":");
            for(Beer beer: brewer.getBeers()){
                System.out.println(beer.getName());
            }

        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
