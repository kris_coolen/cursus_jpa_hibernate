package be.kriscoolen.hoofdstuk6.opdracht9.apps;

import be.kriscoolen.hoofdstuk6.opdracht9.entity.Book;
import be.kriscoolen.hoofdstuk6.opdracht9.entity.CD;
import be.kriscoolen.hoofdstuk6.opdracht9.entity.DVD;
import be.kriscoolen.hoofdstuk6.opdracht9.entity.EBook;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.time.LocalDate;

public class GetMultiMedia {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Book book = em.find(Book.class,1L);
            tx.commit();
            System.out.println(book);

        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
