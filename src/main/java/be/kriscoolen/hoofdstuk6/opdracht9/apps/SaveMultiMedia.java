package be.kriscoolen.hoofdstuk6.opdracht9.apps;

import be.kriscoolen.hoofdstuk6.opdracht9.entity.*;

import javax.persistence.*;

import java.time.LocalDate;

public class SaveMultiMedia {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Book book = new Book("Lord of the rings: the felloship of the ring","J.R.R Tolkien",
                    LocalDate.of(1954,7,29),"George Allen & Unwin",423);
            System.out.println(book);
            EBook eBook = new EBook("Harry Potter and the Philosopher's stone",
                    "J.K. Rowling",
                    LocalDate.of(1997,6,26),
                    "Bloomsbury",223,
                    "https://www.amazon.com/dp/B019PIOJYU/ref=rdr_kindle_ext_tmb#reader_B019PIOJYU");
            System.out.println(eBook);
            CD cd = new CD("Nevermind","Nirvana",LocalDate.of(1991,9,24),
                    "DGC Records",49*60+7,13);
            System.out.println(cd);
            DVD dvd = new DVD("The godfather trilogy, the coppola restoration",
                    "Coppola",
                    LocalDate.of(2019,10,1),"Universal Pictures",545,1);
            System.out.println(dvd);
            em.persist(book);
            em.persist(eBook);
            em.persist(cd);
            em.persist(dvd);
            tx.commit();

        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
