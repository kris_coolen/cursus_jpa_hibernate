package be.kriscoolen.hoofdstuk6.opdracht9.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@Inheritance(strategy = InheritanceType.JOINED)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name="type",discriminatorType = DiscriminatorType.STRING)
public abstract class MultiMedia {
    @Id
  //  @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GeneratedValue(strategy = GenerationType.TABLE,generator = "MultimediaGenerator")
    @TableGenerator(name="MultimediaGenerator",table="MultimediaSequence")
    private long id;

    private String title;
    private String author;
    private LocalDate pubDate;
    private String publisher;

    public MultiMedia() {
    }

    public MultiMedia(String title, String author, LocalDate pubDate, String publisher) {
        this.title = title;
        this.author = author;
        this.pubDate = pubDate;
        this.publisher = publisher;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDate getPubDate() {
        return pubDate;
    }

    public void setPubDate(LocalDate pubDate) {
        this.pubDate = pubDate;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return  "MULTIMEDIA\n"+
                "\ttitle='" + title + '\'' +
                "\n\tauthor='" + author + '\'' +
                "\n\tpubDate=" + pubDate +
                "\n\tpublisher='" + publisher + '\'';
    }
}
