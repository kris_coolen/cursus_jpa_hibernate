package be.kriscoolen.hoofdstuk6.opdracht9.entity;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public abstract class AudioVisual extends MultiMedia {

    private long duration;

    public AudioVisual(){

    }

    public AudioVisual(String title, String author, LocalDate pubDate, String publisher, long duration) {
        super(title, author, pubDate, publisher);
        this.duration = duration;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return super.toString()+"\nAUDIOVISUAL\n\tduration='" + duration + '\'';
    }
}
