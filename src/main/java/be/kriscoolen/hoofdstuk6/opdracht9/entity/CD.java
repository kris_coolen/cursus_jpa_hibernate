package be.kriscoolen.hoofdstuk6.opdracht9.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("CD")
public class CD extends AudioVisual {

    private int tracks;

    public CD(){}

    public CD(String title, String author, LocalDate pubDate, String publisher, long duration, int tracks) {
        super(title, author, pubDate, publisher, duration);
        this.tracks = tracks;
    }

    public int getTracks() {
        return tracks;
    }

    public void setTracks(int tracks) {
        this.tracks = tracks;
    }

    @Override
    public String toString() {
        return super.toString()+"\nCD\n\ttracks='" + tracks + '\'';
    }
}
