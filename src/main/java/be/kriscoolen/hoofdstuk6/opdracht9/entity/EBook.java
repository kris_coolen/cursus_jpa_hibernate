package be.kriscoolen.hoofdstuk6.opdracht9.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("EBOOK")
public class EBook extends Book {
    private String url;

    public EBook(){
    }

    public EBook(String title, String author, LocalDate pubDate, String publisher, int pages, String url) {
        super(title, author, pubDate, publisher, pages);
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }



    @Override
    public String toString() {
        return super.toString()+"\nEBOOK\n\turl='" + url + '\'';
    }
}
