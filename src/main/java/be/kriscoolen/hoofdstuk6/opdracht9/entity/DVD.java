package be.kriscoolen.hoofdstuk6.opdracht9.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("DVD")
public class DVD extends AudioVisual {
    private int subtitles;

    public DVD(){}

    public DVD(String title, String author, LocalDate pubDate, String publisher, long duration, int subtitles) {
        super(title, author, pubDate, publisher, duration);
        this.subtitles = subtitles;
    }

    public int getSubtitles() {
        return subtitles;
    }

    public void setSubtitles(int subtitles) {
        this.subtitles = subtitles;
    }

    @Override
    public String toString() {
        return super.toString()+"\nDVD\n\tsubtitles='" + subtitles + '\'';
    }


}
