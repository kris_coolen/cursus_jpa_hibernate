package be.kriscoolen.hoofdstuk6.opdracht9.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("BOOK")
public class Book extends MultiMedia {
    private int pages;

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public Book(){
    }

    public Book(String title, String author, LocalDate pubDate, String publisher, int pages) {
        super(title, author, pubDate, publisher);
        this.pages = pages;
    }

    @Override
    public String toString() {
        return super.toString()+"\nBOOK\n\tpages='" + pages + '\'';
    }
}
