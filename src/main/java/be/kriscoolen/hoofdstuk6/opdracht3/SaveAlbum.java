package be.kriscoolen.hoofdstuk6.opdracht3;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class SaveAlbum {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            Album album = new Album();
            album.setAuthor("Nirvana");
            album.setTitle("Nevermind");
            List<String> tracks = new ArrayList<>();
            tracks.add("Smells Like Teen Spirit");
            tracks.add("In Bloom");
            tracks.add("Come as You Are");
            tracks.add("Breed");
            tracks.add("Lithium");
            tracks.add("Poly");
            tracks.add("Territorial Pissings");
            tracks.add("Drain You");
            tracks.add("Lounge Act");
            tracks.add("Stay Away");
            tracks.add("On a Plain");
            tracks.add("Something in the Way");
            album.setTracks(tracks);
            tx.begin();
            em.persist(album);
            tx.commit();
            Album album2 = new Album();
            album2.setTitle("Bleach");
            album2.setAuthor("Nirvana");
            List<String> tracks2 = new ArrayList<>();
            tracks2.add("Blew");
            tracks2.add("Floyd the Barber");
            tracks2.add("About a Girl");
            tracks2.add("School");
            tracks2.add("Love Buzz");
            tracks2.add("Paper Cuts");
            tracks2.add("Negative Creep");
            tracks2.add("Scoff");
            tracks2.add("Swap Meet");
            tracks2.add("Mr. Moustache");
            tracks2.add("Sifting");
            album2.setTracks(tracks2);
            tx.begin();
            em.persist(album2);
            tx.commit();
        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }

}
