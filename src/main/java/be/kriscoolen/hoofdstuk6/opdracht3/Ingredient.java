package be.kriscoolen.hoofdstuk6.opdracht3;

import javax.persistence.Embeddable;

@Embeddable
public class Ingredient {
    private int quantity;
    private String name;

    public Ingredient() {
    }

    public Ingredient(String name, int quantity){
        this.quantity = quantity;
        this.name=name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "quantity=" + quantity +
                ", name='" + name + '\'' +
                '}';
    }
}
