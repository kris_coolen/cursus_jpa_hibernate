package be.kriscoolen.hoofdstuk6.opdracht3;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;

public class SaveMenu {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            Menu menu = new Menu();
            menu.setTitle("vol-au-vent");
            ArrayList<Ingredient> ingredients= new ArrayList<>();
            ingredients.add(new Ingredient("kip",1));
            ingredients.add(new Ingredient("parijse champignon",250));
            ingredients.add(new Ingredient("kalfsgehakt",200));
            ingredients.add(new Ingredient("bouquet marmite",1));
            ingredients.add(new Ingredient("roux",1));
            ingredients.add(new Ingredient("room",100));
            menu.setIngredients(ingredients);
            tx.begin();
            em.persist(menu);
            tx.commit();

        } finally {
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
