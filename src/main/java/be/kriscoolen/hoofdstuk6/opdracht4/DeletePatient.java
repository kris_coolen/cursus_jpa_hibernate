package be.kriscoolen.hoofdstuk6.opdracht4;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DeletePatient {

    public static void main(String[] args) {
    EntityManagerFactory emf = null;
    EntityManager em = null;
        try{
        emf = Persistence.createEntityManagerFactory("course");
        em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Patient patient = em.getReference(Patient.class,2L);
        if(patient!=null) System.out.println(patient);
        /*if(patient!=null) {
            em.remove(patient); //because of cascading cascadeType.REMOVE the medical file is also deleted!
            System.out.println("patient " + patient.getName() + " removed from DB!");
        }*/
        patient.removeMedicalFile();
        tx.commit();
        tx.begin();
        patient = em.getReference(Patient.class,2L);
        tx.commit();
        if(patient!=null) System.out.println(patient); //wegens orphanremoval zou medical file van patient weg moeten zijn...
        }finally{
        if(em!=null) em.close();
        if(emf!=null) emf.close();
    }
}
}
