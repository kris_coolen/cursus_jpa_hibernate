package be.kriscoolen.hoofdstuk6.opdracht4;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class SavePatient {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            Patient patient = new Patient();
            patient.setName("Daniel Bijnens");
            MedicalFile medicalFile = new MedicalFile();
            medicalFile.setHeight(1.80f);
            medicalFile.setWeight(96.5f);
            patient.addMedicalFile(medicalFile);

            tx.begin();
            em.persist(patient);
           // em.persist(medicalFile); niet nodig want cascade ingesteld voor persist!
            tx.commit();

        } finally {
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
