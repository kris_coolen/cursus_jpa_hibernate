package be.kriscoolen.hoofdstuk6.opdracht4;

import javax.persistence.*;

@Entity
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REMOVE},fetch = FetchType.LAZY,orphanRemoval = true)
    @JoinColumn(name="MF_ID")
    private MedicalFile medicalFile;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MedicalFile getMedicalFile() {
        return medicalFile;
    }

    public void setMedicalFile(MedicalFile medicalFile) {
        this.medicalFile = medicalFile;
    }

    public void addMedicalFile(MedicalFile mf){
        setMedicalFile(mf);
        mf.setPatient(this);
    }

    public void removeMedicalFile(){
        if(medicalFile != null){
            medicalFile.setPatient(null);
            medicalFile = null;
        }
    }

    @Override
    public String toString() {
        return "Patient{" +
                "name='" + name + '\'' +
                ", medicalFile=" + medicalFile +
                '}';
    }
}
