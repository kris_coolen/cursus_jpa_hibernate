package be.kriscoolen.hoofdstuk6.opdracht4;

import be.kriscoolen.hoofdstuk6.opdracht3.Menu;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GetPatient {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Patient patient = em.find(Patient.class,2L);
            //System.out.println(patient.getMedicalFile().getHeight());
            tx.commit();
            em.detach(patient); //remove patient from persistence context
            System.out.println(patient.getMedicalFile().getHeight());
            if(patient!=null){
                System.out.println(patient);
            }else{
                System.out.println("patient is not found");
            }

        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
