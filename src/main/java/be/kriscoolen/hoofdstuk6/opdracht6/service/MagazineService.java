package be.kriscoolen.hoofdstuk6.opdracht6.service;

import be.kriscoolen.hoofdstuk6.opdracht6.domain.Magazine;
import be.kriscoolen.hoofdstuk6.opdracht6.domain.Reader;
import be.kriscoolen.hoofdstuk6.opdracht6.repository.MagazineDAO;
import be.kriscoolen.hoofdstuk6.opdracht6.repository.ReaderDAO;

public class MagazineService {

    private MagazineDAO magazineDAO;

    public MagazineService(){
        magazineDAO = new MagazineDAO();
    }


    public Magazine create(Magazine magazine){
        return magazineDAO.create(magazine);
    }

    public Magazine findById(long id){
        return magazineDAO.findById(id);
    }
}
