package be.kriscoolen.hoofdstuk6.opdracht6.service;

import be.kriscoolen.hoofdstuk6.opdracht6.domain.Magazine;
import be.kriscoolen.hoofdstuk6.opdracht6.domain.Reader;
import be.kriscoolen.hoofdstuk6.opdracht6.repository.ReaderDAO;

public class ReaderService {
    private ReaderDAO readerDAO;

    public ReaderService(){
        readerDAO = new ReaderDAO();
    }


    public Reader create(Reader reader){
        return readerDAO.create(reader);
    }

    public Reader findById(long id){
        return readerDAO.findById(id);
    }

    public void addMagazine(Reader reader, Magazine magazine){

    }
}
