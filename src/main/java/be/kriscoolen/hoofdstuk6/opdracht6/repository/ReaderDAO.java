package be.kriscoolen.hoofdstuk6.opdracht6.repository;

import be.kriscoolen.hoofdstuk6.opdracht6.domain.Magazine;
import be.kriscoolen.hoofdstuk6.opdracht6.domain.Reader;
import be.kriscoolen.persistence.PersistenceManager;

import javax.persistence.*;

public class ReaderDAO {

    EntityManagerFactory entityManagerFactory = null;
    EntityManager entityManager = null;
    EntityTransaction entityTransaction = null;

    public ReaderDAO(){
        entityManagerFactory =PersistenceManager.getInstance().getFactory("course");
    }


    public Reader create(Reader reader){
        try{
            openConnection();
            entityTransaction.begin();
            entityManager.persist(reader);
            entityTransaction.commit();
        }catch(Exception e){
            entityTransaction.rollback();
            e.printStackTrace();
        }
        finally{
            closeConnection();
        }

        return reader;

    }

    public Reader findById(long id){
        Reader reader = null;
        try{
            openConnection();
            entityTransaction.begin();
            reader = entityManager.find(Reader.class,id);
            entityTransaction.commit();
        }
        catch(Exception e){
            entityTransaction.rollback();
            e.printStackTrace();
        }
        finally{
            closeConnection();
        }
        return reader;
    }

    public void addMagazine(Reader reader, Magazine magazine){

    }


    public void openConnection(){
        entityManager = entityManagerFactory.createEntityManager();
        entityTransaction = entityManager.getTransaction();
    }

    public void closeConnection(){
        if(entityManager!=null) entityManager.close();
    }
}
