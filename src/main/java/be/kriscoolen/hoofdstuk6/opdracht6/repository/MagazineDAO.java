package be.kriscoolen.hoofdstuk6.opdracht6.repository;

import be.kriscoolen.hoofdstuk6.opdracht6.domain.Magazine;
import be.kriscoolen.hoofdstuk6.opdracht6.domain.Reader;
import be.kriscoolen.persistence.PersistenceManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

public class MagazineDAO {

    EntityManagerFactory entityManagerFactory = null;
    EntityManager entityManager = null;
    EntityTransaction entityTransaction = null;

    public MagazineDAO(){
        entityManagerFactory = PersistenceManager.getInstance().getFactory("course");
    }


    public Magazine create(Magazine magazine){
        try{
            openConnection();
            entityTransaction.begin();
            entityManager.persist(magazine);
            entityTransaction.commit();
        }catch(Exception e){
            entityTransaction.rollback();
            e.printStackTrace();
        }
        finally{
            closeConnection();
        }
        return magazine;

    }

    public Magazine findById(long id){
        Magazine magazine = null;
        try{
            openConnection();
            entityTransaction.begin();
            magazine = entityManager.find(Magazine.class,id);
            entityTransaction.commit();
        }
        catch(Exception e){
            entityTransaction.rollback();
            e.printStackTrace();
        }
        finally{
            closeConnection();
        }
        return magazine;
    }

    public void openConnection(){
        entityManager = entityManagerFactory.createEntityManager();
        entityTransaction = entityManager.getTransaction();
    }

    public void closeConnection(){
        if(entityManager!=null) entityManager.close();
    }
}
