package be.kriscoolen.hoofdstuk6.opdracht6.repository;

import javax.persistence.*;

public class MemberOrganisationDAO {

    EntityManagerFactory entityManagerFactory = null;
    EntityManager entityManager = null;
    EntityTransaction entityTransaction = null;




    public void openConnection(){
        entityManagerFactory = Persistence.createEntityManagerFactory("course");
        entityManager = entityManagerFactory.createEntityManager();
        entityTransaction = entityManager.getTransaction();
    }

    public void closeConnection(){
        if(entityManager!=null) entityManager.close();
        if(entityManagerFactory!=null) entityManagerFactory.close();
    }
}

