package be.kriscoolen.hoofdstuk6.opdracht6.domain;

import javax.persistence.*;
import java.util.*;

@Entity
public class Reader {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Magazine> magazines = new HashSet<>();

    public Reader(){

    }

    public Reader(String name){
        this.name=name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Magazine> getMagazines() {
        return magazines;
    }

    public void setMagazines(Set<Magazine> magazines) {
        this.magazines = magazines;
    }

    @Override
    public String toString() {
        return "Reader{" +
                "name='" + name + '\'' +
                ", magazines=" + magazines +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reader)) return false;
        Reader reader = (Reader) o;
        return getName().equals(reader.getName()) &&
                Objects.equals(getMagazines(), reader.getMagazines());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getMagazines());
    }
}
