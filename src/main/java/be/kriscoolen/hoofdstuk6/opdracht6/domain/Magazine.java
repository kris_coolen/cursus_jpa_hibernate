package be.kriscoolen.hoofdstuk6.opdracht6.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Magazine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    //@Column(unique = true)
    private String title;

    public Magazine(){

    }

    public Magazine(String title){
        this.title=title;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Magazine)) return false;
        Magazine magazine = (Magazine) o;
        return getTitle().equals(magazine.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle());
    }
}
