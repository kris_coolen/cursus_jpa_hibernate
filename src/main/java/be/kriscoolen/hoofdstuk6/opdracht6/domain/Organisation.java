package be.kriscoolen.hoofdstuk6.opdracht6.domain;

import javax.persistence.*;
import java.util.*;

@Entity
public class Organisation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToMany(mappedBy = "organisations",cascade = CascadeType.ALL)
    private Set<Member> members = new HashSet<>();

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Member> getMembers() {
        return members;
    }

    public void setMembers(Set<Member> members) {
        this.members = members;
    }

    public void addMember(Member member){
        if(member!=null) {
            members.add(member);
            member.getOrganisations().add(this);
        }
    }

    public void removeMember(Member member){
        if(member!=null){
            members.remove(member);
            member.getOrganisations().remove(this);
        }
    }
}
