package be.kriscoolen.hoofdstuk6.opdracht6.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.*;

@Entity
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToMany
    @JoinTable(name="ORG_MEMBER",
            joinColumns = @JoinColumn(name="MEM_ID"),
            inverseJoinColumns = @JoinColumn(name="ORG_ID"))
    private Set<Organisation> organisations = new HashSet<>();

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Organisation> getOrganisations() {
        return organisations;
    }

    public void setOrganisations(Set<Organisation> organisations) {
        this.organisations = organisations;
    }

    public void addOrganisation(Organisation organisation){
        if(organisation!=null){
            organisations.add(organisation);
            organisation.getMembers().add(this);
        }
    }

    public void removeOrganisation(Organisation organisation){
        if(organisation!=null){
            organisations.remove(organisation);
            organisation.getMembers().remove(this);
        }
    }
}
