package be.kriscoolen.hoofdstuk6.opdracht6.app;

import be.kriscoolen.hoofdstuk6.opdracht6.domain.Magazine;
import be.kriscoolen.hoofdstuk6.opdracht6.domain.Reader;
import be.kriscoolen.hoofdstuk6.opdracht6.service.MagazineService;
import be.kriscoolen.hoofdstuk6.opdracht6.service.ReaderService;

import java.util.ArrayList;
import java.util.List;


public class ReaderMagazineApplication {

    private List<Magazine> magazineList;



    public ReaderMagazineApplication(){
        magazineList = new ArrayList<>();
    }

    public void createReaders(){
        Reader reader1 = new Reader("Kris Coolen");
        Reader reader2 = new Reader("Vera Paumen");
       /* reader.getMagazines().add(magazineList.get(0));
        reader.getMagazines().add(magazineList.get(1));
        reader.getMagazines().add(magazineList.get(2));
        reader.getMagazines().add(magazineList.get(5));*/
        ReaderService service = new ReaderService();
        service.create(reader1);
        service.create(reader2);
    }

    public void createMagazines(){
        magazineList.add(new Magazine("Humo"));
        magazineList.add(new Magazine("Flair"));
        magazineList.add(new Magazine("Playboy"));
        magazineList.add(new Magazine("Breien"));
        magazineList.add(new Magazine("Wonen"));
        magazineList.add(new Magazine("Knack"));
        MagazineService service = new MagazineService();
        for(Magazine magazine: magazineList){
            service.create(magazine);
        }
    }



    public static void main(String[] args) {
        ReaderMagazineApplication application = new ReaderMagazineApplication();
        application.createMagazines();
        application.createReaders();
        // Reader readerDB = application.getDao().findReader(reader.getId());
           // System.out.println(readerDB);
    }
}
