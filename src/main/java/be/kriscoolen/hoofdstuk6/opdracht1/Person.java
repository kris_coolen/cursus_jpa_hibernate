package be.kriscoolen.hoofdstuk6.opdracht1;

import be.kriscoolen.hoofdstuk6.opdracht2.Address;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

@Entity
@Table(name="PERSONS",
        indexes = {@Index(name="LAST_NAME_INDEX",columnList = "LAST_NAME"),
                @Index(name="BIRTHDAY_INDEX",columnList = "BIRTHDAY")})
@SecondaryTable(name="URLS")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private long id;

    @Version
    @Column(name="VERSION")
    private long version;

    @Column(name="FIRST_NAME",length = 40, nullable = false)
    private String firstName;

    @Column(name="LAST_NAME",length = 40, nullable = false)
    private String lastName;

    @Column(name="BIRTHDAY")
    private LocalDate birthday;

    @Column(name="GENDER",length = 10,nullable = false,updatable = false)
    @Enumerated(EnumType.STRING)
    private GenderType gender;

    @Column(name = "PICTURE")
    @Basic(fetch = FetchType.LAZY)
    @Lob
    private byte[] picture;

    @Column(name = "COMMNT")
    @Basic(fetch = FetchType.LAZY)
    @Lob
    private String comment;

    @Column(name ="MARRIED")
    private boolean married;

    @Transient
    private int age;

    @Column(table="URLS",name="HOMEPAGE")
    private String homepage;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="street",column=@Column(name="ADDRESS_STREET")),
            @AttributeOverride(name="number",column=@Column(name="ADDRESS_NUMBER")),
            @AttributeOverride(name="zipCode",column=@Column(name="ADDRESS_ZIPCODE")),
            @AttributeOverride(name="city",column=@Column(name="ADDRESS_CITY")),
            @AttributeOverride(name="country",column=@Column(name="ADDRESS_COUNTRY"))


    })
    private Address address = new Address();

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public long getId() {
        return id;
    }


    public long getVersion() {
        return version;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    @Transient
    public int getAge() {
        LocalDate now = LocalDate.now();
        return (int) ChronoUnit.YEARS.between(birthday,now);
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", version=" + version +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", gender=" + gender +
               // ", picture=" + Arrays.toString(picture) +
                ", comment='" + comment + '\'' +
                ", married=" + married +
                ", age=" + age +
                ", homepage='" + homepage + '\'' +
                ", address=" + address +
                '}';
    }
}
