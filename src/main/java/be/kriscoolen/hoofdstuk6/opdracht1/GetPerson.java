package be.kriscoolen.hoofdstuk6.opdracht1;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GetPerson {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Person person = em.find(Person.class,7L);
            if(person!=null) {
                System.out.println(person);
            }
        }finally{
                if(em!=null) em.close();
                if(emf!=null) emf.close();
            }
    }
}
