package be.kriscoolen.hoofdstuk6.opdracht1;

import be.kriscoolen.hoofdstuk6.opdracht2.Address;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;

public class SavePerson {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            Person person = new Person();
            person.setFirstName("kris");
            person.setLastName("coolen");
            person.setBirthday(LocalDate.of(1985,02,13));
            person.setMarried(true);
            person.setGender(GenderType.MALE);
            Address address = new Address();
            address.setStreet("Moorskampweg");
            address.setNumber("1");
            address.setZipCode("3680");
            address.setCity("MAASEIK");
            address.setCountry("BE");
            person.setAddress(address);
            try {
                File image = new File("C:\\Users\\Java01\\CursusJPaHibernate\\src\\main\\resources\\images\\Kris200.png");
                FileInputStream inputStream = new FileInputStream(image);
                person.setPicture(inputStream.readAllBytes())   ;
            } catch (IOException exception){
                exception.printStackTrace();
            }

            tx.begin();
            em.persist(person);
            tx.commit();
        }
        finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
