package be.kriscoolen.hoofdstuk4.opdracht1;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.InputMismatchException;
import java.util.Scanner;

public class SaveVisitor {
    public static void main(String[] args) {
        int age;
        String name;
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            Visitor visitor = new Visitor();
            Scanner keyboard = new Scanner(System.in);
            System.out.println("what is your age?");
            while(true) {
                try {
                    age = keyboard.nextInt();
                    if(age<0){
                        System.out.println("please enter a valid age. Try again!");
                    }
                    else {
                        break;
                    }

                } catch (InputMismatchException ime) {
                    System.out.println("please enter a valid age. Try again!");
                }
                finally{
                    keyboard.nextLine();
                }
            }
            visitor.setAge(age);
            System.out.println("What is your name?");
            while(true){
                name=keyboard.nextLine();
                if(!name.isBlank()) break;
                else System.out.println("please enter a valid name. Try again!");
            }
            visitor.setName(name);
            em.persist(visitor);
            et.commit();
            System.out.println("visitor added");
        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
