package be.kriscoolen.hoofdstuk3.opdracht1.messages;

import org.hibernate.query.Query;

import javax.persistence.*;
import java.util.List;

public class GetMessage {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            TypedQuery<Message> query = em.createQuery("select m from Message as m",Message.class);
            List<Message> messages = query.getResultList();
            System.out.println(emf.getCache().contains(Message.class,3L));
            for(Message m: messages){
                System.out.println(m.getText());
            }
           /* Message message = em.find(Message.class, 3L);
            System.out.println(emf.getCache().contains(Message.class,3L));
            System.out.println(emf.getCache().contains(Message.class,1L));
            emf.getCache().evict(Message.class,3L);
            System.out.println(emf.getCache().contains(Message.class,3L));
            if(message!=null)
                System.out.println(message.getText());
            else
                System.out.println("message not found in db");*/
            tx.commit();
        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
