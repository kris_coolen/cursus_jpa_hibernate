package be.kriscoolen.hoofdstuk3.opdracht1.messages;
import javax.persistence.*;
import java.util.Objects;

@Entity
public class Message {
    private long id;
    private String text;

    public Message() {
    }

    public Message(long id, String text){
        this.id=id;
        this.text=text;
    }

    @Id
    public long getId(){
        return id;
    }

    public void setId(long id){
        this.id=id;
    }

    public String getText(){
        return text;
    }

    public void setText(String text){
        this.text=text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message = (Message) o;
        return getId() == message.getId() &&
                Objects.equals(getText(), message.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getText());
    }
}
