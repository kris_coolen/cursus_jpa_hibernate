package be.kriscoolen.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceManager {

    private static EntityManagerFactory entityManagerFactory = null;

    private PersistenceManager(){

    }

    private static class SingletonHelper{
        private static final PersistenceManager INSTANCE = new PersistenceManager();
    }

    public static PersistenceManager getInstance(){
        return SingletonHelper.INSTANCE;
    }

    private static void buildFactory(String persistenceUnit){
        try{
            entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnit);

        }catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public EntityManagerFactory getFactory(String persistenceUnit){
        buildFactory(persistenceUnit);
        return entityManagerFactory;
    }

    public void close(){
        try{
            if(entityManagerFactory.isOpen()||entityManagerFactory!=null){
                entityManagerFactory.close();
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        finally {
            entityManagerFactory = null;
        }
    }
}
