package be.kriscoolen.hoofdstuk5.opdracht1;

import be.kriscoolen.hoofdstuk4.opdracht1.Visitor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ChangeVisitor {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        long id;
        Visitor visitor = null;
        String newName;
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();
            while(true){
                System.out.println("Enter visitor id of visitor whose name you would like to change.");
                try{
                    id = keyboard.nextLong();
                    //here we are certain that id is a long!

                    visitor = em.find(Visitor.class,id);
                    if(visitor==null){
                        System.out.println("No visitor with id " + id + " in table Visitor of DB. Try again!");
                    }
                    else{
                        break;
                    }

                }
                catch(InputMismatchException ime){
                    System.out.println("Please enter an integer number. Try again!");
                }
                finally{
                    keyboard.nextLine();
                }
            }
            //change name of visitor
            String oldName = visitor.getName();
            System.out.println("Current visitor name is '" + oldName+ "'. Please choose a new visitor name.");
            do {
                newName = keyboard.nextLine();
                if(newName.isBlank()){
                    System.out.println("Please enter a valid name! Try again.");
                }
                else{
                    break;
                }
            }while(true);

           // entityTransaction.commit();
           // em.close();
           // em.clear();
            //em.detach(visitor);
            visitor.setName(newName);
            System.out.println("name '"+oldName+"' is successfully changed into '" + newName + "'!");
            entityTransaction.commit();
            em.close();
        }finally{
            if(emf!=null) emf.close();
        }
    }
}
