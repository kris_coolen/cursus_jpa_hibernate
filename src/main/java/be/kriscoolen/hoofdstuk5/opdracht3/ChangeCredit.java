package be.kriscoolen.hoofdstuk5.opdracht3;

import javax.persistence.*;
import javax.swing.*;

public class ChangeCredit implements Runnable {

    public void changeCredit(){
        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;
        try{
            entityManagerFactory = Persistence.createEntityManagerFactory("course");
            entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            Credit credit = entityManager.find(Credit.class,1L);
            transaction.commit();
            if(credit!=null){
                String creditChangeString;
                int creditChange;
                do{
                    creditChangeString=JOptionPane.showInputDialog("current balance: "+credit.getBalance()+". Change with:");
                    try{
                        creditChange=Integer.parseInt(creditChangeString);
                        if(credit.getBalance()+creditChange<0){
                            JOptionPane.showMessageDialog(null,
                                    "Insufficient balance: you can withdraw at most €" + credit.getBalance() + ". Try again.",
                                    "Warning", JOptionPane.WARNING_MESSAGE);
                        }
                        else {
                            break;
                        }
                    }
                    catch(NumberFormatException nfe) {
                        JOptionPane.showMessageDialog(null,
                                "Please enter a positive or negative number. Try again.",
                                "Invalid input", JOptionPane.ERROR_MESSAGE);
                    }
                }while(true);
                transaction.begin();
                credit.setBalance(credit.getBalance()+creditChange);
                transaction.commit();
            }
            else{
                System.out.println("there is no credit with id 1!");
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if(entityManager!=null) entityManager.close();
            if(entityManagerFactory!=null) entityManagerFactory.close();
        }
    }
    public static void main(String[] args) {

    }

    @Override
    public void run() {
        changeCredit();
    }
}
