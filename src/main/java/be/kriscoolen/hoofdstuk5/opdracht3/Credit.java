package be.kriscoolen.hoofdstuk5.opdracht3;

import javax.persistence.*;

@Entity
public class Credit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private double balance;
    @Version
    private int version;

    public Credit(){}
    public Credit(double balance){
        setBalance(balance);
    }

    public void setBalance(double balance){
        if(balance<0) throw new IllegalArgumentException("balance cannot be negative!");
        else this.balance=balance;
    }

    public double getBalance(){
        return this.balance;
    }

    public long getId(){
        return this.id;
    }

    public int getVersion() {
        return version;
    }
}
