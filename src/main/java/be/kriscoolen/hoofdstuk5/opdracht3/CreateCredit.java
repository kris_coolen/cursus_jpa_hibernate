package be.kriscoolen.hoofdstuk5.opdracht3;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class CreateCredit {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;
        try{
            entityManagerFactory = Persistence.createEntityManagerFactory("course");
            entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction entityTransaction = entityManager.getTransaction();
            entityTransaction.begin();
            Credit credit = new Credit(100);
            entityManager.persist(credit);
            entityTransaction.commit();
        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(entityManager!=null) entityManager.close();
            if(entityManagerFactory!=null) entityManagerFactory.close();

        }
    }
}
