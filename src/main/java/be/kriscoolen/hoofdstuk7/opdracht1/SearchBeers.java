package be.kriscoolen.hoofdstuk7.opdracht1;

import be.kriscoolen.hoofdstuk6.opdracht8.beers.*;

import javax.persistence.*;
import java.util.List;

public class SearchBeers {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try{
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            //TypedQuery<Beer> query = em.createQuery("select b from Beer as b",Beer.class);
            //TypedQuery<Beer> query = em.createNamedQuery("findAllBeers",Beer.class);
            TypedQuery<Beer> query = em.createNamedQuery("Beer.findAllBeers",Beer.class);

            query.setFirstResult(0);
            query.setMaxResults(20);
            List<Beer> beers = query.getResultList();
            tx.commit();
            for(Beer b: beers){
                System.out.println(b);
            }
        }finally{
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }
    }
}
